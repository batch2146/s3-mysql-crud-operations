USE blog_db;

-- Insert users
INSERT INTO users (email, password, datetime_created) VALUES (
    "johnsmith@gmail.com", "passwordA", "2021-01-01 1"
);
INSERT INTO users (email, password, datetime_created) VALUES (
    "juandelacruz@gmail.com", "passwordB", "2021-01-01 2"
);
INSERT INTO users (email, password, datetime_created) VALUES (
    "janesmith@gmail.com", "passwordC", "2021-01-01 3"
);
INSERT INTO users (email, password, datetime_created) VALUES (
    "mariadelacruz@gmail.com", "passwordD", "2021-01-01 4"
);
INSERT INTO users (email, password, datetime_created) VALUES (
    "johndoe@gmail.com", "passwordE", "2021-01-01 5"
);

-- Insert posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
    1, "First Code", "Hello World!", "2021-01-02 1"
);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
    1, "Second Code", "Hello Earth!", "2021-01-02 2"
);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
    2, "Third Code", "Welcome to Mars!", "2021-01-02 3"
);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES(
    4, "Fourth Code", "Bye bye solar system!", "2021-01-02 4"
);

-- show all posts of author_id = 1
SELECT author_id, title, content FROM posts WHERE author_id = 1;

-- show all users
SELECT email, datetime_created FROM users;

-- update a post
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- delete a user
DELETE FROM users WHERE email = "johndoe@gmail.com";